public class Bankomat {
    int remainingSum; // сумма оставшихся денег в банкомате
    int maxPermittedSum; // максимальная сумма, разрешенная к выдаче
    int maxSum; // максимальная сумма, которая может быть в банкомате
    int countOperation; // количество проведенных операций

    Bankomat(int remainingSum, int maxPermittedSum, int maxSum, int countOperation) {
        this.remainingSum = remainingSum;
        this.maxPermittedSum = maxPermittedSum;
        this.maxSum = maxSum;
        this.countOperation = countOperation;
    }

    public int giveMoney(int give) { // метод выдать деньги
        this.countOperation++;
        if (give > remainingSum || give > maxPermittedSum) {
            return 0;
        }
        remainingSum = remainingSum - give;
        return give;
    }

    public int putMoney(int put) { // метод положить деньги
        this.countOperation++;
        if (put + remainingSum > maxSum) {
            int oldBalance = remainingSum;
            remainingSum = maxSum;
            return put + oldBalance - maxSum;
        }
        remainingSum = remainingSum + put;
        return 0;
    }
}