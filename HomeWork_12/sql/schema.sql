create table driver (
                        id bigserial primary key,
                        first_name char(20),
                        last_name char(20),
                        tel_number integer,
                        experience integer,
                        age integer check (age >=18 and age <=80) not null,
                        driver_license bool,
                        category char(3),
                        rating integer
);

create table car(
                    id bigserial primary key,
                    id_driver integer not null,
                    model char (15),
                    color char (15),
                    number char(10)
);

create table trip(
                     id bigserial primary key,
                     id_car integer not null,
                     foreign key (id_car) references driver(id),
                     foreign key (id_car) references car(id),
                     data_trip timestamp,
                     lasting_trip integer
);
