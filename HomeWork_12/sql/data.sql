insert into driver (first_name, last_name, tel_number, experience, age, driver_license, category, rating)
values ('ivan','ivanov',111111,10,45,true,'B',4);
insert into driver (first_name, last_name, tel_number, experience, age, driver_license, category, rating)
values ('petr','petrov',121112,5,35,true,'D',5);
insert into driver (first_name, last_name, tel_number, experience, age, driver_license, category, rating)
values ('igor','igorev',113113,1,20,true,'C',0);
insert into driver (first_name, last_name, tel_number, experience, age, driver_license, category, rating)
values ('fedor','fedorov',141114,8,35,true,'D',2);
insert into driver (first_name, last_name, tel_number, experience, age, driver_license, category, rating)
values ('sidor','sidorov',115115,6,28,true,'BC',3);


insert into car (id_driver, model, color, number)
values (1,'audi','black','a001aa01');
insert into car (id_driver, model, color, number)
values (2, 'bmw','green','a002ab02');
insert into car (id_driver, model, color, number)
values (3, 'vaz','red','a003ac03');
insert into car (id_driver, model, color, number)
values (4, 'volvo','yellow','a004ad04');
insert into car (id_driver, model, color, number)
values (5, 'renault','black','a005ae05');


insert into trip (id_car, data_trip,lasting_trip)
values (1,'2022-11-13','5');
insert into trip (id_car, data_trip,lasting_trip)
values (2,'2022-11-13','6');
insert into trip (id_car, data_trip,lasting_trip)
values (3,'2022-11-13','7');
insert into trip (id_car, data_trip,lasting_trip)
values (4,'2022-11-13','8');
insert into trip (id_car, data_trip,lasting_trip)
values (5,'2022-11-13','9');



