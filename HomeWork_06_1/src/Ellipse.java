public class Ellipse extends Circle {
    double smallRadius = radius/2;
    double bigRadius = radius;

    public Ellipse(int x, int y, double smallRadius, double bigRadius) {
        super(x,y,bigRadius );
        this.smallRadius = smallRadius;
        this.bigRadius = bigRadius;
    }

    @Override
    public double perimeterFigure() {
        return 4 * (Math.PI * smallRadius * bigRadius + (bigRadius - smallRadius)) / bigRadius + smallRadius;
    }

    @Override
    public double areaFigure() {
        return Math.PI * smallRadius * bigRadius;
    }
}
