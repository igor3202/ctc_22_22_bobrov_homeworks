public class Circle extends Figure {
    protected double radius;
    public Circle(int x, int y, double radius) {
        super( x,y );
        this.radius = radius;
    }
    @Override
    public double perimeterFigure() {
        return 2 * Math.PI * radius;
    }

    @Override
    public double areaFigure() {
        return Math.PI * radius * radius;
    }
}
