public abstract class Figure {
    int x = 0;
    int y = 0;
    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public abstract double areaFigure();

    public abstract double perimeterFigure();

    public void move(int toX, int toY) {
        this.x = toX;
        this.y = toY;
    }
}


