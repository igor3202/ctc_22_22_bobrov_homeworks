public class Main {
    public static void main(String[] args) {
        Square square = new Square(5,6,4);
        Rectangle rectangle = new Rectangle(5, 6, 4,6);
        Circle circle = new Circle(5,6,4);
        Ellipse ellipse = new Ellipse(5, 6, 3, 5);
        Figure[] figure = new Figure[]{square, rectangle, circle, ellipse};
    }
}