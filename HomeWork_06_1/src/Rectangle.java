public class Rectangle extends Square {
    int lenghtSideB;

    public Rectangle(int x, int y, int lenghtSideA, int lenghtSideB) {
       super(x, y, lenghtSideA);
       this.lenghtSideB = lenghtSideB;
    }

    @Override
    public double perimeterFigure() {
        return (lenghtSideA + lenghtSideB) * 2;
    }

    @Override
    public double areaFigure() {
        return lenghtSideA * lenghtSideB;
    }
}
