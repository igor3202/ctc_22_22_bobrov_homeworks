public class Square extends Figure {
   int  lenghtSideA;

    public Square(int x, int y, int lenghtSideA) {
        super(x, y);
        this.lenghtSideA = lenghtSideA;
    }

    @Override
    public double perimeterFigure() {
        return lenghtSideA * 4;
    }
    @Override
    public double areaFigure() {
        return lenghtSideA * lenghtSideA;
    }
}
