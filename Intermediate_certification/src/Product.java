import java.util.Objects;
    public class Product {
        private Integer id;
        private String name;
        private Double price;
        private Integer count;

        public Product(Integer id, String name, Double price, Integer count) {
            this.id = id;
            this.name = name;
            this.price = price;
            this.count = count;
        }
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Product product = (Product) o;
            return id.equals(product.id) && name.equals(product.name) && price.equals(product.price) &&
                    count.equals(product.count);
        }
        public int hashCode() {
            return Objects.hash(id, name, price, count);
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {

            this.name = name;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        @Override
        public String toString() {
            return "Pro.Product{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", cost=" + price +
                    ", count=" + count +
                    '}';
        }
    }



