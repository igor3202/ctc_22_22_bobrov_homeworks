import java.io.IOException;
import java.util.List;

public interface ProductsRepository {
    Product findById (Integer id) throws IOException;
    List<Product> findAllByTitleLike (String title)throws IOException ;
    void  update (Product product) throws IOException;
}
