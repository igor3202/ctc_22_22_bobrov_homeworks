import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpl("input.txt");
        try {
            Product milk = productsRepository.findById(1);
            System.out.println(milk);
            System.out.println("==============");
            System.out.println(productsRepository.findAllByTitleLike("ол"));
            System.out.println("==============");
            System.out.println(productsRepository.findAllByTitleLike("оло"));
            System.out.println("==============");
            milk.setPrice(1000.0);
            productsRepository.update(milk);
            System.out.println(productsRepository.findById(1));
        } catch (IOException ex) {

            {
                System.out.println("Error");
            }
        }
    }
}

