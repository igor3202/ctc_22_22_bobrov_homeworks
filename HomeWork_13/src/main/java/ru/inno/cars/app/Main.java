package ru.inno.cars.app;

import com.zaxxer.hikari.HikariDataSource;
import ru.inno.cars.models.Car;
import ru.inno.cars.repository.CarsRepository;
import ru.inno.cars.repository.CarsRepositoryJdbcImpl;
import ru.inno.cars.repository.CarsService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        String action = null ;
        Properties dbProperties = new Properties();
        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Main.class.getResourceAsStream("/db.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));

        CarsRepository carsRepository = new CarsRepositoryJdbcImpl(dataSource);
        CarsService carsService = new CarsService(carsRepository);
        if (action.equals("read")) {
            List<Car> cars = carsRepository.findAll();
            for (Car car : cars) {
                System.out.println(car);
            }
        }else if  (action.equals("write")) {
                Scanner scanner = new Scanner(System.in);
                while (true) {
                    System.out.println("Введите модель");
                    String model = scanner.nextLine();
                    System.out.println("Введите цвет");
                    String color = scanner.nextLine();
                    System.out.println("Введите номер");
                    String number = scanner.nextLine();
                    Car car = Car.builder().model(model).colorCar(color).numberCar(number).build();
                    carsService.write(car);
                    String field = scanner.nextLine();
                    if (field.equalsIgnoreCase("exit")) {
                        return;
                    }
                }
            }
        }
    }


