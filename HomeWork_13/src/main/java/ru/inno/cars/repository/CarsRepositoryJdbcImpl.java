package ru.inno.cars.repository;

import ru.inno.cars.models.Car;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class CarsRepositoryJdbcImpl implements CarsRepository {
    private static final String SQL_SELECT_ALL = "select * from car order by id";
    private static final String SQL_INSERT = "insert into car (model, color_car, number_car)" +
            "values (?, ?, ?)";

    final DataSource dataSource;

    public CarsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    private static final Function<ResultSet, Car> userRowMapper = row -> {
        try {
            return Car.builder()
                    .idCars(row.getLong("id_cars"))
                    .model(row.getString("model"))
                    .colorCar(row.getString("color_car"))
                    .numberCar(row.getString("number_car"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };

    @Override
    public List<Car> findAll() {
        List<Car> cars = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
                while (resultSet.next()) {
                    Car car = userRowMapper.apply(resultSet);
                    cars.add(car);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        return cars;
    }

    @Override
    public void save() {

    }

    @Override
    public void save(Car cars) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT,
                     Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, cars.getModel());
            preparedStatement.setString(2, cars.getColorCar());
            preparedStatement.setString(3, cars.getNumberCar());
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert cars");
            }
            try (ResultSet generatedId = preparedStatement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    cars.setIdCars(generatedId.getLong("id_cars"));
                } else {
                    throw new SQLException("Can't obtain generated id");
                }
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}


