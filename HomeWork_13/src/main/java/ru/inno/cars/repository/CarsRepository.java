package ru.inno.cars.repository;
import ru.inno.cars.models.Car;
import java.util.List;
public interface CarsRepository {

    List<Car> findAll();

    void save();

    void save(Car cars);
}

