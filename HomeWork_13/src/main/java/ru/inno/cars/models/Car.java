
package ru.inno.cars.models;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Car {
    private Long idCars;
    private String model;
    private String colorCar;
    private String numberCar;

}



