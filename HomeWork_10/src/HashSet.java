public class HashSet<V> implements Set<V> {

    private static final Object PRESENT = new Object();
    private HashMap<V, Object> map = new HashMap<>();

    @Override
    public int put(V v) {
        map.put(v, PRESENT);
        return 0;
    }

    @Override
    public boolean contains(V v) {
        return map.containsKey(v);
    }
}

