import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        HashMap<String, Integer> map = new HashMap<>();

        String input = scanner.nextLine();

        String[] texts = input.split(" ");

        int count = 0;
        String KeeperString = null;

        for (String text : texts) {
            int value = 0;
            if (map.get(text) != null) {
                value = map.get(text);
            }
            int valueElement = value + 1;
            map.put(text, valueElement);
            if (valueElement > count) {
                count = valueElement;
                KeeperString = text;
            }
        }

        if (KeeperString == null || KeeperString.equals("")) {
            System.err.println("Введите текст !");
        } else {
            System.out.println(KeeperString + " " + count);
        }
    }
}

