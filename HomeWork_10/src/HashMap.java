public class HashMap<K, V> implements Map<K, V> {

    private static int DEFAULT_ARRAY_SIZE = 10;

    // массив связных списков
    private Entry<K, V>[] table = new Entry[DEFAULT_ARRAY_SIZE];
    private String[] m;

    private static class Entry<K, V> {
        int hash;
        K key;
        V value;
        Entry<K, V> next;

        public Entry(int hash, K key, V value) {
            this.hash = hash;
            this.key = key;
            this.value = value;
        }
    }


    @Override
    public String put(K key, V value) {
        // получили индекс, куда нужно положить новую пару ключ-значение
        // на основе хеш-кода и длины таблицы
        int hashCode = key.hashCode();
        int index = hashCode & table.length - 1;
        // есть ли уже элемент в этой позиции?
        if (table[index] == null) {
            // если элемента нет - просто создаем новый узел в этой позиции
            table[index] = new Entry<>(hashCode, key, value);
        } else {
            // если там уже что-то было - мы должны проверить весь список (bucket)
            // нет ли там уже такого ключа
            Entry<K, V> current = table[index];

            while (current != null) {
                // если мы нашли узел с точно таким же ключом
                if (current.hash == hashCode && current.key.equals(key)) {
                    // заменить значение
                    current.value = value;
                    return null;
                } // если не нашли - то идем до конца
                current = current.next;
            }
            // если мы с вами дошли до конца, но так и не нашли нужный ключ
            // создаем новый узел и добавляем его (добавляем в начало, в реальном HashMap - в конец)
            Entry<K,V> entry = new Entry<>(hashCode, key, value);
            entry.next = table[index];
            table[index] = entry;
        }

        return null;
    }

    @Override
    public V get(K key) {
        int hashCode = key.hashCode();
        // выясняем, где находится ключ
        int index = hashCode & table.length - 1;

        // беру начало списка
        Entry<K, V> current = table[index];

        // проходим по всему списку
        while (current != null) {
            // если мы нашли узел current, в котором ключ key совпал с искомым ключом
            // сначала сравниваем по хеш-коду, затем, если хеш-коды совпали - по equals
            if (current.hash == hashCode && current.key.equals(key)) {
                // возвращаем значение из этого узла
                return current.value;
            }
            // если этот ключ не совпал, то идем дальше
            current = current.next;
        }
        // если так и не нашли ключ
        return null;

    }

    @Override
    public boolean containsKey(K key) {
        int hashCode = key.hashCode();
        // выясняем, где находится ключ
        int index = hashCode & table.length - 1;

        // беру начало списка
        Entry<K, V> current = table[index];
        // пока не посмотрим весь Bucket
        while (current != null) {
            // смотрим текущий ключ
            if (current.hash == hashCode && current.key.equals(key)) {
                // если нашли ключ, сразу возвращаем true
                return true;
            }
            // идем дальше, если ключ еще не нашли
            current = current.next;
        }

        return false;

    }
}

