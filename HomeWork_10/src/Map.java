public interface Map<K, V> {
    String put(K key, V value);

    V get(K key);

    boolean containsKey(K key);
}

