public interface Set<V> {
    int put(V v);

    boolean contains(V v);
}

