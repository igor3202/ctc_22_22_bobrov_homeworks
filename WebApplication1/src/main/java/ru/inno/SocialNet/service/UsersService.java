package ru.inno.SocialNet.service;

import ru.inno.SocialNet.dto.UserForm;
import ru.inno.SocialNet.model.User;

import java.util.List;

public interface UsersService {
    List<User> getAllUsers();

    void addUser(UserForm user);

    User getUser(Long id);

    void updateUser(Long userId, UserForm user);

    void deleteUser(Long userId);
}
