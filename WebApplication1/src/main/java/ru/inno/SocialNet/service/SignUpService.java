package ru.inno.SocialNet.service;

import ru.inno.SocialNet.dto.UserForm;

public interface SignUpService {
    void signUp(UserForm userForm);
}

