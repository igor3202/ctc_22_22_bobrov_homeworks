package ru.inno.SocialNet.service;

import ru.inno.SocialNet.dto.NetsForm;
import ru.inno.SocialNet.model.Net;
import ru.inno.SocialNet.model.Chat;
import ru.inno.SocialNet.model.User;

import java.util.List;

public interface NetsService {
    List<Net>getAllCourses();
    void addStudentToCourse(Long courseId, Long studentId);

    Net getCourse(Long courseId);

    List<User> getNotInCourseStudents(Long courseId);

    List<User> getInCourseStudents(Long courseId);

    void deleteCourse(Long courseId);

    void addCourse(NetsForm course);

    void updateCourse(Long courseId, NetsForm course);
    void addLessonToCourse(Long courseId, Long lessonId);
    List<Chat> getNotInCourseLessons();
    List<Chat> getInCourseLessons(Long courseId);
}
