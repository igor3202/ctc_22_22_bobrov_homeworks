package ru.inno.SocialNet.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.SocialNet.repositories.NetRepository;
import ru.inno.SocialNet.repositories.ChatsRepository;
import ru.inno.SocialNet.repositories.UsersRepository;
import ru.inno.SocialNet.dto.NetsForm;
import ru.inno.SocialNet.model.Net;
import ru.inno.SocialNet.model.Chat;
import ru.inno.SocialNet.model.User;
import ru.inno.SocialNet.service.NetsService;
import java.util.List;

@RequiredArgsConstructor
@Service
public class NetsServiceImpl implements NetsService {

    private final NetRepository coursesRepository;
    private final UsersRepository usersRepository;
    private final ChatsRepository lessonsRepository;

    @Override
    public List<Net> getAllCourses() {
        return coursesRepository.findAllByStatusNot(Net.Status.DELETED);
    }

    @Override
    public void addStudentToCourse(Long courseId, Long studentId) {
        Net course = coursesRepository.findById(courseId).orElseThrow();
        User student = usersRepository.findById(studentId).orElseThrow();

        student.getCourses().add(course);

        usersRepository.save(student);
    }
    @Override
    public void addLessonToCourse(Long courseId, Long lessonId) {
        Net course = coursesRepository.findById(courseId).orElseThrow();
        Chat lesson = lessonsRepository.findById(lessonId).orElseThrow();
        lesson.setCourse(course);
        lessonsRepository.save(lesson);

    }
    @Override
    public Net getCourse(Long courseId) {
        return coursesRepository.findById(courseId).orElseThrow();
    }

    @Override
    public List<User> getNotInCourseStudents(Long courseId) {
        Net course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesNotContains(course);
    }

    @Override
    public List<User> getInCourseStudents(Long courseId) {
        Net course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesContains(course);
    }

    @Override
    public void deleteCourse(Long courseId) {
        Net courseForDelete = coursesRepository.findById(courseId).orElseThrow();
        courseForDelete.setStatus(Net.Status.DELETED);

        coursesRepository.save(courseForDelete);
    }

    @Override
    public void addCourse(NetsForm course) {
        Net newCourse = Net.builder()
                .title(course.getTitle())
                .description(course.getDescription())
                .status(Net.Status.NOT_CONFIRMED)
                .build();
        coursesRepository.save(newCourse);
    }

    @Override
    public void updateCourse(Long courseId, NetsForm updateDate) {
        Net courseForUpdate = coursesRepository.findById(courseId).orElseThrow();
        courseForUpdate.setTitle(updateDate.getTitle());
        courseForUpdate.setDescription(updateDate.getDescription());
        courseForUpdate.setStart(updateDate.getStart());
        courseForUpdate.setFinish(updateDate.getFinish());
        coursesRepository.save(courseForUpdate);
    }


    @Override
    public List<Chat> getNotInCourseLessons() {
        return lessonsRepository.findAllByCourseNull();
    }

    @Override
    public List<Chat> getInCourseLessons(Long courseId) {
        Net course = coursesRepository.findById(courseId).orElseThrow();
        return lessonsRepository.findAllByCourse(course);
    }
}
