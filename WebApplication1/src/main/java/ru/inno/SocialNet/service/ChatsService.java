package ru.inno.SocialNet.service;

import ru.inno.SocialNet.dto.ChatForm;
import ru.inno.SocialNet.model.Chat;

import java.util.List;

public interface ChatsService {
    List<Chat>getAllLessons();
    Chat getLesson(Long lessonId);
    void deleteLesson(Long lessonId);
    void addLesson(ChatForm lesson);
    void updateLesson(Long lessonId, ChatForm lesson);

}
