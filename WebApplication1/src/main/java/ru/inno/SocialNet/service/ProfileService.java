package ru.inno.SocialNet.service;

import ru.inno.SocialNet.model.User;
import ru.inno.SocialNet.security.details.CustomUserDetails;

public interface ProfileService {
    User getCurrent(CustomUserDetails userDetails);
}

