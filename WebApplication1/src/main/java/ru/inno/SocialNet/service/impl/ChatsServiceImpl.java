package ru.inno.SocialNet.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.SocialNet.repositories.ChatsRepository;
import ru.inno.SocialNet.dto.ChatForm;
import ru.inno.SocialNet.model.Chat;
import ru.inno.SocialNet.service.ChatsService;

import java.util.List;
@RequiredArgsConstructor
@Service

public class ChatsServiceImpl implements ChatsService {
    private final ChatsRepository lessonsRepository;

    @Override
    public List<Chat> getAllLessons() {
        return lessonsRepository.findAllByStatusNot(Chat.Status.DELETED);
    }

    @Override
    public Chat getLesson(Long lessonId) {
        return lessonsRepository.findById(lessonId).orElseThrow();
    }

    @Override
    public void deleteLesson(Long lessonId) {
        Chat lessonForDelete = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForDelete.setStatus(Chat.Status.DELETED);
        lessonsRepository.save(lessonForDelete);
    }

    @Override
    public void addLesson(ChatForm lesson) {
        Chat newLesson = Chat.builder()
                .name(lesson.getName())
                .summary(lesson.getSummary())
                .startTime(lesson.getStartTime())
                .finishTime(lesson.getFinishTime())
                .status(Chat.Status.NOT_CONFIRMED)
                .build();
        lessonsRepository.save(newLesson);
    }

    @Override
    public void updateLesson(Long lessonId, ChatForm updateDate) {
        Chat lessonForUpdate = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForUpdate.setName(updateDate.getName());
        lessonForUpdate.setSummary(updateDate.getSummary());
        lessonForUpdate.setStartTime(updateDate.getStartTime());
        lessonForUpdate.setFinishTime(updateDate.getFinishTime());
        lessonsRepository.save(lessonForUpdate);
    }
}
