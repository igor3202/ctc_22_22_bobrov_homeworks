package ru.inno.SocialNet.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.SocialNet.model.User;
import ru.inno.SocialNet.repositories.UsersRepository;
import ru.inno.SocialNet.security.details.CustomUserDetails;
import ru.inno.SocialNet.service.ProfileService;

@RequiredArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileService {

    private final UsersRepository usersRepository;

    @Override
    public User getCurrent(CustomUserDetails userDetails) {
        return usersRepository.findById(userDetails.getUser().getId()).orElseThrow();
    }
}


