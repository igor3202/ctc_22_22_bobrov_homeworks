package ru.inno.SocialNet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.SocialNet.model.Net;
import ru.inno.SocialNet.model.Chat;

import java.util.List;

public interface ChatsRepository extends JpaRepository<Chat,Long> {
    List<Chat>findAllByStatusNot(Chat.Status state);

    List<Chat> findAllByCourseNull();

    List<Chat> findAllByCourse(Net course);
}
