package ru.inno.SocialNet.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.SocialNet.model.Net;

import java.util.List;

public interface NetRepository extends JpaRepository<Net, Long> {
    List<Net> findAllByStatusNot(Net.Status status);
}
