package ru.inno.SocialNet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.SocialNet.model.Net;
import ru.inno.SocialNet.model.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByStateNot(User.State state);

    List<User> findAllByCoursesNotContains(Net course);

    List<User> findAllByCoursesContains(Net section);

    Optional<Object> findByEmail(String email);
}
