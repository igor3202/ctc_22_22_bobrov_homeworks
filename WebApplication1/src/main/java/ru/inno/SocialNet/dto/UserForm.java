package ru.inno.SocialNet.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserForm {

    private String firstName;
    private String lastName;
    private String email;
    private Integer age;

    public CharSequence getPassword() {
        return null;
    }
}
