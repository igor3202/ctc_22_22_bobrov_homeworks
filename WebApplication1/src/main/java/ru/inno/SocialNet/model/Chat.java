package ru.inno.SocialNet.model;

import javax.persistence.*;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;

import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = "course")
@ToString(exclude = "course")
@DynamicInsert
public class Chat {
    public enum Status {
        NOT_CONFIRMED, DELETED
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    @Column(length = 1000)
    private String summary;

    @Column(name = "start_time")
    private LocalTime startTime;

    @Column(name = "finish_time")
    private LocalTime finishTime;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Net course;
    @Enumerated(value = EnumType.STRING)
    private Chat.Status status;
}
