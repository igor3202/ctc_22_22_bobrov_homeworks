public class OddNumberPrintTask extends AbstractNumberPrintTask {
    public OddNumberPrintTask(int from, int to) {
        super(from, to);
    }
    @Override
    public void complete() {
        int i;
        for (i = from; i <= to; i++) {
            if (i % 2 == 1) {
                System.out.println(i);
            }
        }
    }
}
