public abstract class AbstractNumberPrintTask implements Task {
    int from;
    int to;
    @Override
    public abstract void complete();

    public AbstractNumberPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }
}
