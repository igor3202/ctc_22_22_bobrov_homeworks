public class Main {
    public static void main(String[] args) {
        Task[] tasks;
        tasks = new Task[]{ new EvenNumbersPrintTask(1, 20), new OddNumberPrintTask(30, 50)};
        completeAllTasks(tasks);
    }
    private static void completeAllTasks(Task[] tasks) {
        for (Task task : tasks) {
            task.complete();
        }
    }
}
