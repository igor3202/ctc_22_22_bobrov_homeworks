public class EvenNumbersPrintTask extends AbstractNumberPrintTask {
    public EvenNumbersPrintTask(int from, int to) {
        super(from, to);
    }
    @Override
    public void complete() {
        int i;
        for (i = from; i < to; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }
}
