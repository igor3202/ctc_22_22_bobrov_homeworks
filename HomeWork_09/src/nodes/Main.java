package nodes;

public class Main {
    public static void main(String[] args) {
        Node<String> a = new Node<>("Marsel");
        Node<String> b = new Node<>("Sidikov");
        Node<String> c = new Node<>("Java");
        Node<String> d = new Node<>("PHP");

        a.next = b;
        b.next = c;
        c.next = d;

        Node<String> current = a;

        while (current != null) {
            System.out.println(current.value);
            current = current.next; // i++
        }
    }
}

