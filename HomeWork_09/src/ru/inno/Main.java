package ru.inno;



public class Main {

    public static String mergeDocuments(Iterable<String> documents) {
        StringBuilder mergedDocument = new StringBuilder();

        Iterator<String> documentsIterator = documents.iterator();

        while (documentsIterator.hasNext()) {
            mergedDocument.append(documentsIterator.next() + " ");
        }

        return mergedDocument.toString();
    }

    public static void main(String[] args) {
        List<String> stringList = new LinkedList<>();

        stringList.add("Hello!");
        stringList.add("Bye!");
        stringList.add("Fine!");
        stringList.add("C++!");
        stringList.add("PHP!");
        stringList.add("Cobol!");

        stringList.remove("PHP!");

        List<Integer> integerList = new LinkedList<>();
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);

        String documents = mergeDocuments(stringList);
        //   Integer documents = mergeDocuments(integerList);

        System.out.println(documents);

    }
}
