package ru.inno;

public class LinkedList<T> implements List<T> {
    private Node<T> first;

    private Node<T> last;
    private int count;

    private static class Node<T> {
        T value;
        Node<T> next;
        Node<T> prev;

        public Node(T value) {
            this.value = value;
        }
    }

    @Override
    public void add(T element) {
        Node<T> newNode = new Node<>(element);
        Node<T> previousNode = null;
        if (count == 0) {
            first = newNode;
        } else {
            last.next = newNode;
            previousNode = last;
        }
        last = newNode;
        last.prev = previousNode;
        count++;
    }

    @Override
    public void remove(T element) {
        Node<T> current = first; //текущий узел равен первому элементу
        Node<T> first2; //  узел когда первый раз встретилось искомое слово
        Node<T> prev; // предыдущий узел
        while (current != null) {
            if (current.value.equals(element)) {
                first2 = current;
                prev = first2.prev;
                prev.next = first2.next;
                count--;
            }
            current = current.next;
        }
    }

    @Override
    public boolean contains(T element) {
        // получить ссылку на первый элемент списка
        // пройтись по всем элементам списка
        Node<T> current = this.first;
        // пока не обошли весь список
        while (current != null) {
            // если значение в текущем узле совпало с искомым
            if (current.value.equals(element)) {
                return true;
            }
            // если не совпало - идем к следующему узлу
            current = current.next;
        }
        // если не нашли элемент
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeAt(int index) {
        if (index >= 0 && index < count) {
            Node<T> current = first;
            Node<T> previous;
            Node<T> elementA;

            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            elementA = current;
            previous = elementA.prev;
            previous.next = elementA.next;
            count--;
        }
    }

    @Override
    public T get(int index) {
        if (0 <= index && index <= count) {
            // начинаем с первого элемента
            Node<T> current = this.first;

            // если запрошенный индекс был равен - 3
            // 0, 1, 2
            for (int i = 0; i < index; i++) {
                // на каждом шаге цикла двигаемся дальше
                current = current.next;
            }
            return current.value;
        }
        return null;

    }

    // внутренний
    private class LinkedListIterator implements Iterator<T> {

        private Node<T> current = first;

        @Override
        public T next() {
            // берем и запоминаем значение текущего узла
            T value = current.value;
            // сдвигаем current на следующий узел
            current = current.next;
            // возвращаем значение узла
            return value;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator();
    }
}


