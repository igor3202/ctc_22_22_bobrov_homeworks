package ru.inno.webapp.ec.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.webapp.ec.dto.CoursesForm;
import ru.inno.webapp.ec.model.Course;
import ru.inno.webapp.ec.model.Lesson;
import ru.inno.webapp.ec.model.User;
import ru.inno.webapp.ec.repositories.CoursesRepository;
import ru.inno.webapp.ec.repositories.LessonsRepository;
import ru.inno.webapp.ec.repositories.UsersRepository;
import ru.inno.webapp.ec.service.CoursesService;
import java.util.List;

@RequiredArgsConstructor
@Service
public class CoursesServiceImpl implements CoursesService {

    private final CoursesRepository coursesRepository;
    private final UsersRepository usersRepository;
    private final LessonsRepository lessonsRepository;

    @Override
    public List<Course> getAllCourses() {
        return coursesRepository.findAllByStatusNot(Course.Status.DELETED);
    }

    @Override
    public void addStudentToCourse(Long courseId, Long studentId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        User student = usersRepository.findById(studentId).orElseThrow();

        student.getCourses().add(course);

        usersRepository.save(student);
    }
    @Override
    public void addLessonToCourse(Long courseId, Long lessonId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        Lesson lesson = lessonsRepository.findById(lessonId).orElseThrow();
        lesson.setCourse(course);
        lessonsRepository.save(lesson);

    }
    @Override
    public Course getCourse(Long courseId) {
        return coursesRepository.findById(courseId).orElseThrow();
    }

    @Override
    public List<User> getNotInCourseStudents(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesNotContains(course);
    }

    @Override
    public List<User> getInCourseStudents(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesContains(course);
    }

    @Override
    public void deleteCourse(Long courseId) {
        Course courseForDelete = coursesRepository.findById(courseId).orElseThrow();
        courseForDelete.setStatus(Course.Status.DELETED);

        coursesRepository.save(courseForDelete);
    }

    @Override
    public void addCourse(CoursesForm course) {
        Course newCourse = Course.builder()
                .title(course.getTitle())
                .description(course.getDescription())
                .status(Course.Status.NOT_CONFIRMED)
                .build();
        coursesRepository.save(newCourse);
    }

    @Override
    public void updateCourse(Long courseId, CoursesForm updateDate) {
        Course courseForUpdate = coursesRepository.findById(courseId).orElseThrow();
        courseForUpdate.setTitle(updateDate.getTitle());
        courseForUpdate.setDescription(updateDate.getDescription());
        courseForUpdate.setStart(updateDate.getStart());
        courseForUpdate.setFinish(updateDate.getFinish());
        coursesRepository.save(courseForUpdate);
    }


    @Override
    public List<Lesson> getNotInCourseLessons() {
        return lessonsRepository.findAllByCourseNull();
    }

    @Override
    public List<Lesson> getInCourseLessons(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return lessonsRepository.findAllByCourse(course);
    }
}
