package ru.inno.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.ec.models.Course;

import java.util.List;
import java.util.Optional;


public interface CoursesRepository extends JpaRepository<Course, Long> {
    List<Course> findAllByStatusNot(Course.Status status);
}







