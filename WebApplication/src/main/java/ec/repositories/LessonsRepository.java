package ru.inno.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.Lesson;

import java.util.List;

public interface LessonsRepository extends JpaRepository<Lesson,Long> {
    List<Lesson>findAllByStatusNot(Lesson.Status state);

    List<Lesson> findAllByCourseNull();

    List<Lesson> findAllByCourse(Course course);
}

