package ru.inno.webapp.ec.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.webapp.ec.dto.CoursesForm;
import ru.inno.webapp.ec.service.CoursesService;

@RequiredArgsConstructor
@RequestMapping(value ="/courses")
@Controller
public class CoursesController {

    private final CoursesService coursesService;


    @PostMapping("/{course-id}/students")
    public String addStudentToCourse(@PathVariable("course-id") Long courseId,
                                     @RequestParam("student-id") Long studentId) {
        coursesService.addStudentToCourse(courseId, studentId);
        return "redirect:/courses/" + courseId;
    }
    @PostMapping("/{course-id}/lessons")
    public String addLessonToCourse(@PathVariable("course-id") Long courseId,
                                     @RequestParam("lesson-id") Long lessonId) {
        coursesService.addLessonToCourse(courseId, lessonId);
        return "redirect:/courses/" + courseId;
    }

    @GetMapping("/{course-id}")
    public String getCoursePage(@PathVariable("course-id") Long courseId, Model model) {
        model.addAttribute("course", coursesService.getCourse(courseId));
        model.addAttribute("notInCourseStudents", coursesService.getNotInCourseStudents(courseId));
        model.addAttribute("inCourseStudents", coursesService.getInCourseStudents(courseId));
        model.addAttribute("notInCourseLessons", coursesService.getNotInCourseLessons());
        model.addAttribute("inCourseLessons", coursesService.getInCourseLessons(courseId));
        return "course_page";
    }
    @GetMapping
    public String getCoursesPage(@RequestParam(value = "orderBy", required = false) String orderBy,
                               @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("courses", coursesService.getAllCourses());
        return "courses_page";
    }
    @PostMapping
    public String addCourse(CoursesForm course) {
       coursesService.addCourse(course);
        return "redirect:/courses/";
    }
    @GetMapping("/{course-id}/delete")
    public String updateCourse(@PathVariable("course-id") Long courseId) {
        coursesService.deleteCourse(courseId);
        return "redirect:/courses/";
    }
    @PostMapping("/{course-id}/update")
    public String updateCourse(@PathVariable("course-id") Long courseId, CoursesForm course) {
        coursesService.updateCourse(courseId, course);
        return "redirect:/courses/" + courseId;
    }

}
