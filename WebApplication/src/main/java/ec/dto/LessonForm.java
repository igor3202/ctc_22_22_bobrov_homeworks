package ru.inno.ec.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LessonForm {
    private String name;

    private String summary;
    @DateTimeFormat(pattern = "HH:MM")
    private LocalTime startTime;
    @DateTimeFormat(pattern = "HH:MM")
    private LocalTime  finishTime;
}


