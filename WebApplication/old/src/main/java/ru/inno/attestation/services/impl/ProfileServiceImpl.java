package ru.inno.attestation.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.attestation.models.User;
import ru.inno.attestation.repositories.UsersRepository;
import ru.inno.attestation.security.details.CustomUserDetails;
import ru.inno.attestation.services.ProfileService;

@RequiredArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileService {

    private final UsersRepository usersRepository;

    @Override
    public User getCurrent(CustomUserDetails userDetails) {
return usersRepository.findById(userDetails.getUser().getId()).orElseThrow();
    }
}
