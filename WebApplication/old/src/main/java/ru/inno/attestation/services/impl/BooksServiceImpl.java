package ru.inno.attestation.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.attestation.dto.BookForm;
import ru.inno.attestation.models.Book;
import ru.inno.attestation.repositories.BooksRepository;
import ru.inno.attestation.services.BooksService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class BooksServiceImpl implements BooksService {
    private final BooksRepository booksRepository;

    @Override
    public List<Book> getAllBooks() {
        return booksRepository.findAllByStateNot(Book.State.DELETED);
    }

    @Override
    public Book getBook(Long lessonId) {
        return booksRepository.findById(lessonId).orElseThrow();
    }

    @Override
    public void addBook(BookForm lesson) {
        Book newBook = Book.builder()
                .author(lesson.getAuthor())
//                .startTime(lesson.getStartTime())
//                .finishTime(lesson.getFinishTime())
                .state(Book.State.CONFIRMED)
                .summary(lesson.getSummary())
                .build();
        booksRepository.save(newBook);
    }

    @Override
    public void deleteBook(Long lessonId) {
        Book bookForDelete = booksRepository.findById(lessonId).orElseThrow();
        bookForDelete.setState(Book.State.DELETED);

        booksRepository.save(bookForDelete);
    }

    @Override
    public void updateBook(Long lessonId, BookForm updateData) {
        Book bookForUpdate = booksRepository.findById(lessonId).orElseThrow();
//        bookForUpdate.setStartTime(updateData.getStartTime());
//        bookForUpdate.setFinishTime(updateData.getFinishTime());
        bookForUpdate.setSummary(updateData.getSummary());
        booksRepository.save(bookForUpdate);
    }
}
