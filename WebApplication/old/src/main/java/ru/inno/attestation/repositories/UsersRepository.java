package ru.inno.attestation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.attestation.models.Department;
import ru.inno.attestation.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByStateNot(User.State state);

    List<User> findAllByDepartmentNotContains(Department department);

    List<User> findAllByDepartmentContains(Department department);

    Optional<User> findByEmail(String email);
}
