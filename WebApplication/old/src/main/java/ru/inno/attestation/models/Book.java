package ru.inno.attestation.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = "department")
@ToString(exclude = "department")
@Table(name = "book")
public class Book {

    public enum State {
        CONFIRMED, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String author;
    @Column(length = 1000)
    private String summary;

//    @Column(name = "start_time")
//    private LocalTime startTime;
//
//    @Column(name = "finish_time")
//    private LocalTime finishTime;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Department department;

    @Enumerated(value = EnumType.STRING)
    private State state;


}
