package ru.inno.attestation.services;

import ru.inno.attestation.dto.DepartmentForm;
import ru.inno.attestation.models.Department;
import ru.inno.attestation.models.Book;
import ru.inno.attestation.models.User;

import java.util.List;

public interface DepartmentService {
    List<Department> getAllDepartments();

    void addDepartment(DepartmentForm department);

    void addStudentToDepartment(Long departmentId, Long studentId);

    Department getDepartment(Long departmentId);

    List<User> getNotInDepartmentStudents(Long departmentId);

    List<User> getInDepartmentStudents(Long departmentId);

    void deleteDepartment(Long departmentId);

    void updateDepartment(Long departmentId, DepartmentForm department);

    void addLessonToDepartment(Long departmentId, Long bookId);

    List<Book> getNotInDepartmentBooks();

    List<Book> getInDepartmentBooks(Long departmentId);
}
