package ru.inno.attestation.services;

import ru.inno.attestation.dto.BookForm;
import ru.inno.attestation.models.Book;

import java.util.List;

public interface BooksService {

    List<Book> getAllBooks();

    Book getBook(Long bookId);

    void addBook(BookForm book);

    void deleteBook(Long bookId);

    void updateBook(Long bookId, BookForm book);
}
