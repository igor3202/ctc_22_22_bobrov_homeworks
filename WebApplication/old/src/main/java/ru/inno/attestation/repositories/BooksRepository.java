package ru.inno.attestation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.attestation.models.Department;
import ru.inno.attestation.models.Book;

import java.util.List;

public interface BooksRepository extends JpaRepository<Book, Long> {

    List<Book> findAllByStateNot(Book.State state);

    List<Book> findAllByDepartmentNull();

    List<Book> findAllByDepartment(Department department);
}
