package ru.inno.attestation.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.inno.attestation.models.User;
import ru.inno.attestation.security.details.CustomUserDetails;
import ru.inno.attestation.services.ProfileService;

@RequiredArgsConstructor
@Controller
public class ProfileController {

    final private ProfileService profileService;

    @GetMapping("/")
    public  String getRoot() {
        return "redirect:/profile";
    }

    @GetMapping("/profile")
    public  String getProfile(@AuthenticationPrincipal CustomUserDetails userDetails, Model model) {
Long userId = userDetails.getUser().getId();
model.addAttribute("user", profileService.getCurrent(userDetails));
        return "profile_page";
    }
}
