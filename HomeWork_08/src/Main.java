public class Main {
    private static ArraysTaskResolver ArraysTasksResolver;

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int from = 1, to = 3;

        ArrayTask task1 = (x, y, z) -> {
            int count = 0;
            int sum1 = 0;
            for (count = y; count <= z; count++) {
                sum1 += x[count];
            }
            return sum1;
        };
        int result1 = task1.resolve(array, from, to);
        System.out.println(result1);

        ArrayTask task2 = (x, y, z) -> {
            int count = 0;
            int sum2 = 0;
            int maxValue = 0;
            for (count = y; count <= z; count++) {
                if (x[count] > maxValue) {
                    maxValue = x[count];
                }
            }
            while (maxValue != 0) {
                sum2 += (maxValue % 10);
                maxValue /= 10;
            }
            return sum2;
        };
        int result2 = task2.resolve(array, from, to);
        System.out.println(result2);

        SumFromToOfArray sumFromToOfArray = new SumFromToOfArray();
        SumOfDigitFromArray sumOfDigitFromArray = new SumOfDigitFromArray();
        ArraysTaskResolver.resolveTask(array,
                sumFromToOfArray,
                from,
                to);
        ArraysTaskResolver.resolveTask(array,
                sumOfDigitFromArray,
                from,
                to);
    }
    public static void setArraysTasksResolver(ArraysTaskResolver arraysTasksResolver) {
        ArraysTasksResolver = arraysTasksResolver;
    }
}
