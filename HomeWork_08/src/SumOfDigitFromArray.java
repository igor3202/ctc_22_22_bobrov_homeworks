public class SumOfDigitFromArray implements ArrayTask {
         public int task;
        public int resolve(int[] array, int from, int to) {
            int maxValue = 0, task = 0;
            for (int i = from; i <= to; i++) {
                if (array[i] > maxValue) {
                    maxValue = array[i];
                }
            }
            while (maxValue != 0) {
                task += (maxValue % 10);
                maxValue /= 10;
            }
            return task;
        }
    }



